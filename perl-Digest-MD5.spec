%define mod_name Digest-MD5

Name:           perl-%{mod_name}
Version:        2.59
Release:        1
Summary:        Perl interface to the MD5 algorithm
License:        GPL-1.0-or-later OR Artistic-1.0-Perl
URL:            https://metacpan.org/release/%{mod_name}
Source0:        https://cpan.metacpan.org/authors/id/T/TO/TODDR/%{mod_name}-%{version}.tar.gz
BuildRequires:  gcc make perl-devel perl-generators perl-interpreter perl(strict) perl(Config) perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Digest::base) perl(Exporter) perl(vars) perl(XSLoader)
BuildRequires:  perl(Encode) perl(Test) perl(Test::More) perl(warnings) perl(MIME::Base64) perl(threads)
Requires:       perl(Digest::base) perl(XSLoader)

%{?perl_default_filter}

%description
The Digest::MD5 module allows you to use the RSA Data Security Inc. MD5
Message Digest algorithm from within Perl programs. The algorithm takes as
input a message of arbitrary length and produces as output a 128-bit
"fingerprint" or "message digest" of the input.

%package_help

%prep
%setup -q -n %{mod_name}-%{version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PACKLIST=1 OPTIMIZE="$RPM_OPT_FLAGS"
%{make_build}

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc Changes README
%{perl_vendorarch}/auto/*
%{perl_vendorarch}/Digest*

%files help
%{_mandir}/man?/*

%changelog
* Thu Feb 06 2025 sqfu <dev01203@linx-info.com> - 2.59-1
- update version to 2.59
- Remove meaningless const type qualifier to silence HPUX builds.
- remove useless perl 5.6 check
- convert bits.t test to use Test::More
- Update Digest::MD5 Synopsis and Examples. Add `my` to synopsis
- MD5.xs: eliminate C++ guards

* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 2.58-3
- drop useless perl(:MODULE_COMPAT) requirement

* Mon Oct 24 2022 hongjinghao <hongjinghao@huawei.com> - 2.58-2
- use %{mod_name} marco

* Wed Jan 27 2021 liudabo <liudabo1@huawei.com> - 2.58-1
- upgrade version to 2.58

* Sun Sep 29 2019 yefei <yefei25@huawei.com> - 2.55-419
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:delete irrelevant comment

* Thu Sep 14 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.55-418
- Package init
